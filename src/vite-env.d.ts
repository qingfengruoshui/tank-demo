/// <reference types="vite/client" />
interface IModel{
    name:string,
    canvas:ICanvas,
    render():void,
    destroy():void,
    tank?:IModel,
    bulletNum?:number,
    direction:string,
    width:number,
    height:number,
    life?:number,
    step?:number,
    prop?:string[],
    stopMove?:boolean,
    x?:number,
    y?:number,
}
interface IModelController{
    new(x:number,y:number):IModel
}
interface ICanvas{
    num():number,
    model():IModelController,
    intervalId?:number,
    killCount?:number,
    render(),
    models:[],
    ctx:CanvasRenderingContext2D,
    removeModel(model:IModel):void,
    renderModels():void
}
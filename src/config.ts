import straw from './static/images/straw/straw.png';
import water from './static/images/water/water.gif';
import wall from './static/images/wall/wall.gif';
import wallTop from './static/images/wall/wallTop.gif';
import wallLeft from './static/images/wall/wallLeft.gif';
import steel from './static/images/wall/steels.gif';
import tankTop from './static/images/tank/top.gif'
import tankRight from './static/images/tank/right.gif'

//坦克上下左右
import tankBottom from './static/images/tank/bottom.gif'
import tankLeft from './static/images/tank/left.gif'
import bullet from './static/images/bullet/bullet.jpg'
import boss from './static/images/boss/boss.png'
//玩家上下左右
import playerTop from './static/images/player/top.gif';
import playerRight from './static/images/player/right.gif';
import playerBottom from './static/images/player/bottom.gif';
import playerLeft from './static/images/player/left.gif';
import { directionEnum } from './emun/directionEmun';
//music
import addAutio from './static/music/add.wav';
import blastAutio from './static/music/blast.wav';
import fireAutio from './static/music/fire.wav';
import startAutio from './static/music/start.wav';
export default {
    canvas: {
        width: 900,
        height: 600,
        fresh: 20
    },
    model: {
        width: 30,
        height: 30,
    },

    images: {
        straw,
        wall,
        wallTop,
        wallLeft,
        water,
        steel,
        tankTop,
        tankRight,
        tankBottom,
        tankLeft,
        bullet,
        boss,
        playerTop,
        playerRight,
        playerBottom,
        playerLeft

    },
    autios: {
        addAutio,
        blastAutio,
        fireAutio,
        startAutio
    },
    straw: {
        num: 50,
        zindex: 10,
    },
    wall: {
        num: 100,
        zindex: 1,
    },
    water: {
        num: 50,
        zindex: 1,
    },
    steel: {
        num: 20,
        zindex: 1,
    },
    prop: {
        num: 0,
        zindex: 50,
        duration: 30000,
        space:10000,
    },
    tank: {
        num: 10,
        bulletNum: 1, //敌方坦克炮弹总数
        zindex: 2,
        maxNum:5
    },
    bullet: {
        num: 0,
        zindex: 5
    },
    boss: {
        num: 0,
        zindex: 1
    },
    player: {
        hitSelf: false, //攻击自己人
        num: 1, //玩家数量
        zindex: 5,
        life:3,
        bulletNum:3,
        player1: {
            control: {
                [directionEnum.top]: 'w',
                [directionEnum.right]: 'd',
                [directionEnum.bottom]: 's',
                [directionEnum.left]: 'a',
                shoot: 'g'
            }, color: 'rgba(252,196,34, 0.6)'

        },
        player2: {
            control: {
                [directionEnum.top]: 'i',
                [directionEnum.right]: 'l',
                [directionEnum.bottom]: 'k',
                [directionEnum.left]: 'j',
                shoot: ']'
            }, color: 'RGBA(42,156,73,0.6)'

        },

        color: ['rgba(252,196,34, 0.6)', 'RGBA(42,156,73,0.6)', 'RGBA(48,125,239,0.6)', 'RGBA(228,61,48,0.6)']
    }
}
import config from "../config";

type autioMapKey = keyof typeof config.autios;
const autios = new Map<autioMapKey, HTMLAudioElement>()

export const autio = {
    loadPromises: () => {
        //没有顺序的promise队列包裹
        return Object.entries(config.autios).map(([key, path]) => {
            return new Promise((resolve) => {
                const el = document.createElement('audio') as HTMLAudioElement;
                el.src = path;
                el.oncanplaythrough = () => {
                    autios.set(key as autioMapKey, el)
                    resolve(el)
                }
            })
        })

    },
    play(key:autioMapKey){
       
       autios.get(key)?.play();
       console.log(key)
    }
}
import config from "../config";
import _ from 'lodash';
type positionType = {
    x: number,
    y: number
}
class position {
    public collection: positionType[] = [];
    positionCollection(num: number) {
        const collection: positionType[] = []
        Array(num).fill('').map(() => {
            while (true) {
                const position = this.randPosition()
                const exists = this.collection.some(c => { return c.x == position.x && c.y == position.y });
                if (!exists) {
                    this.collection.push(position);
                    collection.push(position)
                    break;
                }
            }
        });

        return collection;
    }
    randPosition() {
        const maxX = Math.floor(config.canvas.width / config.model.width) - 1;
        const maxY = Math.floor(config.canvas.height / config.model.height) - 1 - 2;

        const x = _.random(maxX) * config.model.width;
        const y = _.random(2, maxY) * config.model.height;

        return {
            x, y
        }
    }
}

export default new position()
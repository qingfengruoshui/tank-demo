
import config from "../config";
//批量导入静态图片
type mapKey=keyof typeof config.images //keyof typeof 导出对象中所有的key
export const image=new Map<mapKey, HTMLImageElement>(); 
//自定义无序promise

export const promises=Object.entries(config.images).map(([key ,path])=>{
    return new Promise(resolve=>{
        const img=document.createElement('img');
        img.src=path;
        img.onload=()=>{
            image.set(key as mapKey,img);
            resolve(img);
        }
    })
})
    



import _ from "lodash"
import player from "./canvas/player"
import steel from "./canvas/steel"
import wall from "./canvas/wall"
import water from "./canvas/water"
import config from "./config"
import tank from "./canvas/tank"

export default {
    isCanvasTouch(x: number, y: number, width: number = config.model.width, height: number = config.model.height) {
        return x < 0 ||
            x + width > config.canvas.width ||
            y < 0 ||
            y + height > config.canvas.height
    },
    isModelTouch(
        x: number,
        y: number,
        width: number = config.model.width,
        height: number = config.model.height,
        models = [...wall.models, ...water.models, ...steel.models]
    ) {
        //把some改用find，返回碰触的模型
        return models.find(model => {
            const state = x + width <= model.x! || x >= model.x! + model.width || y + height <= model.y! || y >= model.y! + model.height
            return !state
        })
    },
    speed(model:IModel) {
        //加速
        const modelWidth = config.model.width
        let step = model.step;
        while (true) {
            if (model.step! >= modelWidth / 2) {
                model.step = modelWidth / 2
                return;
            }
            if (modelWidth % step! == 0) {
                model.step = step;
                return
            } else {
                step!++;
            }
        }

    },
    bullet(model:IModel) {
        //子弹
        model.bulletNum!++;
        return
    },
    life(model:any) {
        //生命
        if (model instanceof tank.model()) {
            tank.createSingleModel();
        } else {
            model.life!++;
        }

    },
    blast(model:IModel) {
        //炸弹
        if (model instanceof tank.model()) {
            player.destroyModels();
        } else {
            tank.destroyModels();

        }

    },
    time(model:any) {
        //定时
        if (model instanceof tank.model()) {
            player.models = player.models.map(model => { model.stopMove = true; return model });
            setTimeout(() => {
                player.models = player.models.map(model => { model.stopMove = false; return model });
            }, config.prop.duration);

        } else {
            tank.models = tank.models.map(model => { model.stopMove = true; return model });
            tank.renderModels()
            setTimeout(() => {
                tank.models = tank.models.map(model => { model.stopMove = false; return model });

            }, config.prop.duration);
        }
    },
    home() {
        //护家       
        wall.clearPosModel()
        steel.createBossWall()

        const queque = [[steel, wall], [wall, steel], [steel, wall], [wall, steel],[steel, wall],]
        setTimeout(() => {
            queque.reduce((promise,canvas) => {
                return promise.then(() => {
                    return new Promise(resolve => {
                        setTimeout(() => {
                            canvas[0].clearPosModel();
                            canvas[1].createBossWall();
                            resolve()
                        }, config.prop.space);
                    })

                })
            }, Promise.resolve())
        }, config.prop.duration);
    },
    hack(model:IModel) {
        //无敌
        model.prop?.push('hack')
        setTimeout(() => {
            model.prop = model.prop?.filter(n => n != 'hack')
        }, config.prop.duration);
    },
    boat(model:IModel) {
        //小船
        model.prop?.push('boat')
    }
}
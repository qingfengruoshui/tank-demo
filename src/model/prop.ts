import config from "../config";
import modelAbstract from "./modelAbstract";
import canvas from '../canvas/prop';
import { directionEnum } from "../emun/directionEmun";
import { image } from "../service/image";
import _ from "lodash";
import util from "../util";

export default class extends modelAbstract implements IModel {
    name: string = 'prop';
    canvas: ICanvas = canvas;
    direction: string = directionEnum.bottom;
    width: number = config.model.width;
    height: number = config.model.height / 2;
    toolName: string = ''
    tools = { speed: '加速', boat: '小船', life: '生命', blast: '炸弹', hack: '无敌', bullet: '子弹', home: '护家', time: '定时' }
    image(): HTMLImageElement {
        //暂用文字代替

        return image.get('boss')!;
    }
    constructor(x: number, y: number, toolName = null) {
        super(x, y)
        //随机一个名字
        if (!toolName) {
            const arr=Object.values(this.tools)
            toolName=arr[_.random(arr.length-1)]
            
            this.toolName = toolName
        }
    }

    render(): void {
        this.draw()
    }
    protected draw(): void {
        this.canvas.ctx.fillStyle = 'red'
        this.canvas.ctx.font = '15px CascadiaMono'
        this.canvas.ctx.textAlign = 'center'
        this.canvas.ctx.textBaseline = 'middle'
     
        this.canvas.ctx.fillText(this.toolName, this.x, this.y, config.model.width)
    }
    response(tank) {
        const key=Object.entries(this.tools).find(([key,value])=>{
            return value==this.toolName;
        });
    
        util[key[0]](tank);
        this.destroy()
    }
    destroy(): void {

        super.remove()
    }


} 
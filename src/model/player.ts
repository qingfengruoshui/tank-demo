import modelAbstract from "./modelAbstract";
import canvas from '../canvas/player'
import { image } from '../service/image'
import config from "../config";
import _, { takeWhile } from 'lodash'
import { directionEnum } from '../emun/directionEmun';
import util from "../util";
import wall from "../canvas/wall";
import water from "../canvas/water";
import steel from "../canvas/steel";
import tank from "../canvas/tank";
import bullet from "../canvas/bullet";
import boss from "../canvas/boss";
import { autio } from "../service/autio";
import prop from "../canvas/prop";

export default class player extends modelAbstract {
    image(): HTMLImageElement {
        return image.get("player" + _.upperFirst(this.direction))
    }
    intervalId = 0;
    keyup = []
    keydown = []
    prop = []
    stopMove: boolean = false
    step: number = 3;
    life: number = config.player.life;
    canvas: ICanvas = canvas;
    bulletNum = config.player.bulletNum;
    constructor(public name: string, public x: number, public y: number, public color: string = 'rgba(0,0,0,0)') {
        super(x, y)

    }
    changeDirection(direction: directionEnum) {
        this.direction = direction;
    }
    render(): void {
        this.draw()

    }

    addBullet() {
        bullet.addBullet(this);
        autio.play('fireAutio');
    }
    controller() {
        //按鍵按下控制
        this.keydown.map((item: IModel) => {
            //遍历所有方向集合
            if (Object.keys(directionEnum).includes(item.direction)) {
                this.changeDirection(item.direction)
                if (!this.intervalId) {
                    this.intervalId = setInterval(() => {
                        this.move()
                    }, 50)
                }
            }

            //遍历攻击集合
            if (item.direction == 'shoot') {
                const bulletNum = bullet.models.filter(bullet => bullet.tank == this).length
                if (bulletNum < this.bulletNum) {
                    this.addBullet()
                    this.keydown=this.keydown.filter(keyup=>keyup.direction!='shoot');
                }

            }
        })
        // 按键抬起时清空
    
        this.keydown = this.keydown.filter((item: IModel) => {
            //遍历所有方向集合
            //将keyup的全部清除
            return !this.keyup.some(keyup => {
                return keyup.direction == item.direction 
            });
        })
        this.keyup=[];

        if (!this.keydown.length) {
            clearInterval(this.intervalId)
            this.intervalId=0
        }
        

    }
    protected draw(): void {
        //附加颜色贴图
        super.draw()
        this.canvas.ctx.fillStyle = this.color;
        this.canvas.ctx.fillRect(this.x, this.y, this.width, this.height)
    }
    move() {
        let x = this.x;
        let y = this.y;
        if (this.stopMove) return
        switch (this.direction) {
            case directionEnum.top:
                y -= this.step
                break;
            case directionEnum.right:
                x += this.step
                break;
            case directionEnum.bottom:
                y += this.step
                break;
            case directionEnum.left:
                x -= this.step;
                break;
        }
        const players = this.canvas.models.filter(model => model != this);
        const models = [
            ...wall.models,
            ...steel.models, ...tank.models,
            ...players, ...boss.models,
            ...prop.models
        ]
        //小船
        if (!this.prop.includes('boat' as never)) {
            models.push(...water.models)
        }
        const touchModel = util.isModelTouch(x, y, this.width, this.height, models)
        if (util.isCanvasTouch(x, y) || touchModel) {
            if (touchModel?.name == 'prop') {

                touchModel.response(this)

            }

            return
        } else {
            this.x = x;
            this.y = y;
        }

        this.canvas.renderModels()
    }

    destroy(): void {
        if (this.prop.includes('hack' as never)) return
        this.blast(this)
        super.remove();
    }
    direction?: directionEnum | undefined = directionEnum.top;
    width: number = config.model.width;
    height: number = config.model.height;

}
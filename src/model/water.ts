import config from "../config";
import modelAbstract from "./modelAbstract";
import canvas from '../canvas/water';
import { directionEnum } from "../emun/directionEmun";
import { image } from "../service/image";

export default class extends modelAbstract implements IModel {
    name: string = 'water';
    canvas:ICanvas = canvas;  
    direction: string = directionEnum.bottom;
    width: number = config.model.width;
    height: number = config.model.height;
    
    image(): HTMLImageElement {
        return image.get('water')!;
    }
    render(): void {
        super.draw()
    }
    destroy(): void {
        super.remove()
    }
}
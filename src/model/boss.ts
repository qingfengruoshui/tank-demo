import { image } from "../service/image";
import modelAbstract from "./modelAbstract";
import canvas from '../canvas/boss'
import config from "../config";
import { directionEnum } from "../emun/directionEmun";
export default class extends modelAbstract implements IModel {
    name = 'boss'
    image(): HTMLImageElement {
        return image.get('boss')!
    }
    canvas: ICanvas = canvas;
    render(): void {
        this.draw()
    }

    destroy(): void {
        super.remove()

    }
    direction?: directionEnum;
    width: number = config.model.width;
    height: number = config.model.height;

}
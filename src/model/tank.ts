import config from "../config";
import modelAbstract from "./modelAbstract";
import canvas from '../canvas/tank';
import { directionEnum } from "../emun/directionEmun";
import { image } from "../service/image";
import _ from "lodash";
import util from "../util";
import wall from "../canvas/wall";
import water from "../canvas/water";
import steel from "../canvas/steel";
import player from "../canvas/player";
import boss from "../canvas/boss";
import prop from "../canvas/prop";

export default class extends modelAbstract implements IModel {
    name: string = 'tank';
    canvas: ICanvas = canvas;
    prop = []
    step: number = 1; //坦克移动速度
    direction: string = directionEnum.bottom;
    bulletPromise = Promise.resolve()
    width: number = config.model.width;
    height: number = config.model.height;
    bulletNum: number = config.tank.bulletNum
    stopMove: boolean = false;
    constructor(x: number, y: number) {
        super(x, y);
        this.direction = this.randomDirection()
    }
    render(): void {
        super.draw()
        if (!this.stopMove) {
            this.move()
        }

    }
    destroy(): void {
        if (this.prop.includes('hack' as never)) return
        this.blast(this)
        super.remove()
        this.canvas.killCount!--
    }
    
    protected move() {
        let x = this.x;
        let y = this.y;
        if (_.random(20) == 1) {
            this.direction = directionEnum.bottom
        }
   
        switch (this.direction) {
            case directionEnum.top:
                y -= this.step;
                break;
            case directionEnum.right:
                x += this.step;
                break;
            case directionEnum.bottom:
                y += this.step;
                break;
            case directionEnum.left:
                x -= this.step;
                break;
        }
        //碰撞检测
        const models = [...prop.models, ...wall.models, ...steel.models, ...player.models, ...boss.models]
        if (!this.prop.includes('boat' as never)) {
            models.push(...water.models);
        }
    
        let touchModel = util.isModelTouch(x, y, this.width, this.height, models)
    
        if (util.isCanvasTouch(x, y) || touchModel) {

           
            if (touchModel?.name == 'prop') {
               
                touchModel.response(this)
                
                this.x=x;
                this.y=y;
            } else {
                this.direction = this.randomDirection();

            }

        } else {
            this.x = x;
            this.y = y;
        }
        super.draw();//防止转向时闪烁
    }
    protected randomDirection() {
        return Object.keys(directionEnum)[_.random(3)]
    }
    image(): HTMLImageElement {

        return image.get(("tank" + _.upperFirst(this.direction)) as never)!;
    }

}
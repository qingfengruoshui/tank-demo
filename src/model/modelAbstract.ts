// import { directionEnum } from "../emun/directionEmun";
// import { autio } from "../service/autio";
export default abstract class modelAbstract {
  abstract image(): HTMLImageElement;
  abstract canvas: ICanvas;
  abstract render(): void;
  abstract destroy(): void;
  abstract direction?: string;
  abstract width: number;
  abstract height: number;
  constructor(public x: number, public y: number) {

  }
  protected draw() {
    this.canvas.ctx.drawImage(this.image(), this.x, this.y, this.width, this.height)
  }
  protected remove() {
    //传递给canvas进行具体操作

    this.canvas.removeModel(this);
  }
  protected blast(model:IModel,x=model.x!,y=model.y!): void {
   
    //定义promise队列 嵌套promise外层一直返回promise传递下一个then来处理
    Array(...Array(8).keys()).reduce((promise, n) => {
      return promise.then(() => {
        return new Promise(resolve => {
          setTimeout(() => {

            const img = document.createElement('img');
         
            img.src = `/images/blasts/blast${n}.gif`
            img.onload = () => {
              this.canvas.ctx.drawImage(img,x,y,model.width,model.height)
              resolve(promise)
            }

          }, 10);

        })
      })
    }, Promise.resolve())


  }
}
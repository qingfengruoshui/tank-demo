import modelAbstract from "./modelAbstract";
import canvas from '../canvas/bullet';
import { directionEnum } from "../emun/directionEmun";
import { image } from "../service/image";
import util from "../util";
import wall from "../canvas/wall";
import steel from "../canvas/steel";
import boss from "../canvas/boss";
import straw from "../canvas/straw";
import tank from '../canvas/tank';
import player from "../canvas/player";
import { autio } from "../service/autio";
import config from "../config";

export default class extends modelAbstract implements IModel {

    name: string = 'bullet';
    canvas: ICanvas = canvas;
    step: number = 4; //子弹移动速度

    constructor(public tank: IModel) {
        super(tank.x + tank.width / 2, tank.y + tank.height / 2);
        this.direction = tank.direction
    }

    image(): HTMLImageElement {
        return image.get('bullet')!;
    }

    render(): void {

        this.move()
    }

    destroy(): void {
        super.remove()
    }

    direction: string = directionEnum.bottom;
    width: number = 2;
    height: number = 2;

    protected move() {
        let x = this.x;
        let y = this.y;

        switch (this.direction) {
            case directionEnum.top:
                y -= this.step;
                break;
            case directionEnum.right:
                x += this.step;
                break;
            case directionEnum.bottom:
                y += this.step;
                break;
            case directionEnum.left:
                x -= this.step;
                break;
        }
        //碰撞检测
        const touchModel = util.isModelTouch(
            x,
            y,
            this.width,
            this.height,
            [...wall.models, ...steel.models, ...boss.models, ...straw.models, ...tank.models, ...player.models])
        if (util.isCanvasTouch(x, y, this.width, this.height) || touchModel) {

            if (touchModel) {
                //清除地图模型

                if (touchModel.name == 'wall') {
                    touchModel.life--
                    if (this.direction == directionEnum.top || this.direction == directionEnum.bottom) {
                        this.blast(touchModel, touchModel.x, this.y - touchModel.height / 2)
                        touchModel.height = config.model.height / 2
                        touchModel.direction = directionEnum.top;

                        if (this.direction == directionEnum.bottom) {
                            touchModel.y = touchModel.y + config.model.height / 2
                        }
                    }
                    if (this.direction == directionEnum.left) {
                        touchModel.direction = directionEnum.left;
                        touchModel.width = config.model.width / 2
                        this.blast(touchModel, touchModel.x + touchModel.width / 2)
                    }
                    if (this.direction == directionEnum.right) {
                        touchModel.direction = directionEnum.left;
                        touchModel.width = config.model.width / 2
                        touchModel.x = touchModel.x + config.model.width / 2;
                        this.blast(touchModel, touchModel.x - touchModel.width / 2)
                    }
                    if (touchModel.life == 0) {
                        touchModel.destroy();
                    }
                    touchModel.canvas.renderModels()
                    this.destroy()
                } else if (touchModel.name == 'steel') {
                    if (this.direction == directionEnum.top || this.direction == directionEnum.bottom) {
                        this.blast(touchModel, touchModel.x, this.y - touchModel.height / 2)
                    }
                    if (this.direction == directionEnum.left) {
                        this.blast(touchModel, touchModel.x + touchModel.width / 2)
                    }
                    if (this.direction == directionEnum.right) {
                        this.blast(touchModel, touchModel.x - touchModel.width / 2)
                    }

                    this.destroy()
                } else if (touchModel.name == 'straw') {
                    touchModel.destroy()
                    //草地清除不阻挡子弹
                    this.x = x;
                    this.y = y;
                } else if (touchModel.name == 'boss') {
                    this.blast(touchModel)
                    touchModel.destroy()
                    this.destroy()
                } else if (touchModel.name=='bullet' && this!=touchModel) { 
                    this.destroy;
                    touchModel.destroy;
                }
                else if (this.isSameCamp(touchModel)) {
                    //坦克互战
                    //炮弹不能打同阵营

                    touchModel.destroy()
                    this.destroy()
                    if (touchModel.name != 'tank') {
                       
                        touchModel.canvas.createLife()
                    }
                } else {
                    this.x = x;
                    this.y = y;
                }
            } else {
                this.destroy()
            }

        } else {
            this.x = x;
            this.y = y;
        }
        super.draw();//防止转向时闪烁
    }

    //同阵营
    isSameCamp(touchModel) {
        let modelCamp = touchModel.name;
        let bulletCamp = this.tank.name;
        //炮弹阵营
        if (!config.player.hitSelf) {
            //自己不能打同阵营得
            //剔除序号
            bulletCamp = bulletCamp.replace(/\d+/, '');
            modelCamp = modelCamp.replace(/\d+/, '');
        }

        return bulletCamp.includes(modelCamp) || modelCamp.includes(bulletCamp) ? false : true
    }

}
import config from "../config";
import modelAbstract from "./modelAbstract";
import canvas from '../canvas/straw';
import { directionEnum } from "../emun/directionEmun";
import { image } from "../service/image";

export default class extends modelAbstract implements IModel {
    name: string = 'straw';
    canvas:ICanvas = canvas;
    render(): void {
        super.draw()
    }
    destroy(): void {
        super.remove()
    }
    direction: string = directionEnum.bottom;
    width: number = config.model.width;
    height: number = config.model.height;
    

    image(): HTMLImageElement {
        return image.get('straw')!;
    }

}
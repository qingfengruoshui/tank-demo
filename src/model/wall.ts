import config from "../config";
import modelAbstract from "./modelAbstract";
import canvas from '../canvas/wall';
import { directionEnum } from "../emun/directionEmun";
import { image } from "../service/image";

export default class extends modelAbstract implements IModel {
    name: string = 'wall';
    canvas:ICanvas = canvas;  
    direction: string = directionEnum.bottom;
    life=2;
    width: number = config.model.width;
    height: number = config.model.height;

    image(): HTMLImageElement {
        let imgName
        if(this.life==2){
            imgName='wall'
        }
        if(this.life==1){
            if(this.direction==directionEnum.top || this.direction==directionEnum.bottom){
                imgName='wallTop';
            }else{
                imgName='wallLeft';
            }
        }       
        return image.get(imgName)!;
    }
    render(): void {
        super.draw()
    }
    destroy(): void {
        super.remove()
    }
}
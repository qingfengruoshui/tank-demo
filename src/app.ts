import straw from './canvas/straw'
import './style.scss'
import { promises } from './service/image';
import config from './config'
import tank from './canvas/tank';
import wall from './canvas/wall';
import bullet from './canvas/bullet';
import water from './canvas/water';
import steel from './canvas/steel';
import boss from './canvas/boss';
import player from './canvas/player';
import { autio } from './service/autio';
import prop from './canvas/prop'



export const app = {
    isStart: false,
    intervalId: 0, //监控全局
    playModel: 1, //1合作模式 2交战模式 
    state: 1,  //1开始 0 暂停或结束
    win: 1,
    el: document.querySelector('#app') as HTMLDivElement,
    bootstrap() {
        const bg: HTMLDivElement = document.querySelector('#bg')!
        bg.style.width = config.canvas.width + "px"
        bg.style.height = config.canvas.height + "px"
        bg.style.fontSize = '24px'
        config.player.bulletNum = 10
        this.el?.addEventListener('click', async () => {
            if (!this.isStart) {
                await this.start()
                this.intervalId = setInterval(() => {
                    if (this.state == 1) {
                        if (this.playModel == 1) {
                            if (boss.models.length == 0) {
                                this.win = 0
                                this.state = 0
                            }
                            if (tank.killCount == tank.num()) {
                                this.win = 1
                                this.state = 0
                            }

                            if (player.lifes.every(play=> play.life < 0)) {
                                this.win = 0
                                this.state = 0
                            }
                        }
                        if (this.playModel == 2) {
                            this.state = player.models.length == 1 ? 0 : 1;//只剩下一个玩家
                        }
                        if (this.state == 0) {
                            this.stop()
                        }
                    }


                }, config.canvas.fresh)
            }
        })



    },
    async start() {
        if (this.isStart === true) return
        await Promise.all(promises)
        await Promise.all(autio.loadPromises())
        this.isStart = true;
        autio.play('startAutio')
        this.el.style.backgroundImage = 'none';
        straw.render();
        wall.init().render();
        tank.render();
        bullet.render();
        water.render();
        steel.init().render();
        boss.render();
        player.init().render();
        prop.init().render();

    },
    stop() {

        player.stop()
        tank.stop()
        bullet.stop()
        prop.stop()
        this.text()
    },
    text() {
        const canvas = document.createElement('canvas');
        canvas.width = config.canvas.width
        canvas.height = config.canvas.height
        canvas.style.zIndex = '99';
        let ctx = canvas.getContext('2d')!;

        let msgbox = this.win == 1 ? '恭喜你赢得胜利' : '啥也不是';
        ctx.fillStyle = 'red'
        ctx.font = '80px CascadiaMono'
        ctx.textAlign = 'center'
        ctx.textBaseline = 'middle'
        ctx.fillText(msgbox, config.canvas.width / 2, config.canvas.height / 2);
        this.el.insertAdjacentElement('afterbegin', canvas);
    }
}
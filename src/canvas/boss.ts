import canvasAbstract from "./canvasAbstract";
import model from '../model/boss'
import config from "../config";
class boss extends canvasAbstract implements ICanvas {
    intervalId:number=0
    num(): number {
        return 0
    }
    render(): void {
        this.createBoss()
        this.renderModels()
    }
    createBoss() {
        const pos = {
            x: config.canvas.width / config.model.width / 2 * config.model.width,
            y: (config.canvas.height / config.model.height - 1) * config.model.height
        }
        const className=this.model()
        const instance=new className(pos.x,pos.y) 
        this.models.push(instance)
    }
    model(): IModel {
        return model 
    }

}
export default new boss('boss')
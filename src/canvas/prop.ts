import config from "../config";
import canvasAbstract from "./canvasAbstract";
import model from '../model/prop';
import position from "../service/position";
class prop extends canvasAbstract implements ICanvas {
   intervalId:number=0
   num(): number {
      return 0;
   }
   model(): IModel {
      return model;
   }
   init():this {
      
      const pos = position.positionCollection(1)[0] 
      super.createSingleModel(pos)
      setTimeout(() => {
         this.models=[]; //清空道具
         setTimeout(() => {
            this.init()
         }, config.prop.space);
      }, config.prop.duration);
      
      return this;
   }
   render(): void {
      let n = 0
      this.intervalId = setInterval(() => {
         if (n % 2 == 0) {
            super.renderModels()

         } else {
            super.clearCanvas()
         }
      }, 500)

   }

}
export default new prop('prop')
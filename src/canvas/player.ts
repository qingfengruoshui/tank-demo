import canvasAbstract from "./canvasAbstract"
import model from '../model/player'
import config from "../config"
import { directionEnum } from "../emun/directionEmun"
import bullet from "./bullet";
import { autio } from "../service/autio";
import { app } from "../app";

export default new (class extends canvasAbstract {
    protected bingEvent: boolean = false
    intervalId = 0
    num(): number {
        return config.player.num
    }
    lifes = [];
    init() {
        this.createModels()
        this.event()
        return this
    }
    render(): void {

        this.intervalId = setInterval(() => {
            // 
            this.controllers()
            super.renderModels()
         
        }, config.canvas.fresh)
    }
    protected event() {
        if (!this.bingEvent) {
            document.addEventListener('keydown', (e) => {
                if (app.state == 0) return //暂停或者结束
                this.models.map(model => {
                    const controls = config.player[model.name]['control']
                    for (const [direction, keyup] of Object.entries(controls)) {
                        //move
                        if (keyup == e.key) {
                            model.keydown.push({ direction, keyup: keyup })
                        }
                    }

                })
            })
            document.addEventListener('keyup', (e) => {
                if (app.state == 0) return //暂停或者结束
                this.models.map(model => {
                    const controls = config.player[model.name]['control']
                    for (const [direction, keyup] of Object.entries(controls)) {
                        //move
                        if (keyup == e.key) {
                            model.keyup.push({ direction, keyup: keyup })
                        }
                    }

                })
            })
            this.bingEvent = true
        }
    }
    protected controllers() {
        this.models.map(model => model.controller());
    }
    createLife() {
        let leftNum = 0;
        let rightNum = 0;
        let xNum = config.canvas.width / config.model.width / 2
        let y = ((config.canvas.height / config.model.height) - 1) * config.model.height

        this.lifes.map((play, i) => {

            const isExist = this.models.some(model => model == play)
            if (!isExist) {
                if (play.life >= 0) {

                    let num = 0;
                    if (i % 2 == 0) {
                        num = xNum - 2 - leftNum
                        leftNum++
                    } else {
                        num = xNum + 2 + rightNum
                        rightNum++
                    }
                    const x = num * config.model.width
                    play.x = x;
                    play.y = y;
                    
                    this.models.push(play)
                    play.life--;

                }
            }

        })
    }
    createModels(): void {
        //创建多个玩家
        let xNum = config.canvas.width / config.model.width / 2
        let y = ((config.canvas.height / config.model.height) - 1) * config.model.height
        let leftNum = 0;
        let rightNum = 0;
        for (let i = 0; i < Math.min(this.num(), 4); i++) {

            let num = 0;

            if (i % 2 == 0) {
                num = xNum - 2 - leftNum
                leftNum++
            } else {
                num = xNum + 2 + rightNum
                rightNum++
            }
            const x = num * config.model.width

            let playName = 'player' + (i * 1 + 1) //player1 player2 命名规则

            const isModelExists = this.models.some(model => model.name == playName)
            if (!isModelExists) {

                const instance = new model(playName, x, y, config.player[playName]['color']);
                this.lifes.push(instance);
                instance.life--
                this.models.push(instance)
            }

        }
    }
    model(): IModel {
        return model
    }

})('player')
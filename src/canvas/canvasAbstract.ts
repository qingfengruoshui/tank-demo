import config from "../config";
import position from "../service/position";

export default abstract class canvasAbstract {
    abstract num(): number;
    abstract render(): void;
    abstract model(): IModel;
    public models: IModel[] = []; //存放所有模型

    constructor(
        public name: string, protected app: HTMLDivElement = document.querySelector('#app')!,
        protected canvas: HTMLCanvasElement = document.createElement('canvas'),
        public ctx = canvas.getContext('2d')!) {

        this.createCanvas()
    }
    createCanvas() {
        this.canvas.width = config.canvas.width;
        this.canvas.height = config.canvas.height;
        this.canvas.setAttribute('name', this.name);
        if (config[this.name]) {
            if (config[this.name]['zindex']) {

                this.canvas.style.zIndex = config[this.name]['zindex'];
            }
        }
        this.app.insertAdjacentElement('afterbegin', this.canvas); //插入画布
    }
    //批量处理创建模型，通过模型渲染
    createModels() {
        //遍历num，将对应模型实例化，并存入画布备用       
        position.positionCollection(this.num()).map(pos => {
            this.createSingleModel(pos);
        })
    }
    //指定位置建立模型
    createSingleModel(pos: { x: number, y: number }) {
        const model = this.model();
        const instance = new model(pos.x, pos.y);
        this.models.push(instance);
    }
    getBossWallPos() {

        let walls = []
        const XcenterPos = config.canvas.width / config.model.width / 2
        const YbottomPos = config.canvas.height / config.model.height - 1;
        //相对坐标，画墙
        walls.push(
            { x: XcenterPos - 1, y: YbottomPos },
            { x: XcenterPos - 1, y: YbottomPos - 1 },
            { x: XcenterPos, y: YbottomPos - 1 },
            { x: XcenterPos + 1, y: YbottomPos - 1 },
            { x: XcenterPos + 1, y: YbottomPos })
        walls = walls.map(wall => {
            return { x: wall.x * config.model.width, y: wall.y * config.model.height }
        })
        return walls
    }
    clearPosModel(pos = null) {
        if (!pos) {
            pos = this.getBossWallPos();
        }
        let xArr=pos.map(p=>p.x)
        let yArr=pos.map(p=>p.y)
     
        const maxPosX=Math.max(...xArr)
        const minPosX=Math.min(...xArr)
        const maxPosY=Math.max(...yArr)
        const minPosY=Math.min(...yArr)
        

        this.models = this.models.filter(model => {

            const isExists = model.x<=maxPosX && model.x>=minPosX && model.y<=maxPosY && model.y>=minPosY
            
            return !isExists ;
        })
    
        this.renderModels()
    }
    renderModels() {
        this.clearCanvas()
        this.models.map(model => {
            model.render()
        })
    }

    destroyModels() {
        this.models.map(model => model.destroy());
    }
    protected clearCanvas() {
        this.ctx.clearRect(0, 0, config.canvas.width, config.canvas.height)
    }
    removeModel(model: IModel) {
        this.models = this.models.filter(m => m != model);
        this.renderModels()
    }
    stop() {
        clearInterval(this.intervalId)
    }
}
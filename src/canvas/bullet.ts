
import canvasAbstract from "./canvasAbstract";
import model from '../model/bullet';
import tank from "./tank";
import config from "../config";

class bullet extends canvasAbstract implements ICanvas {
    intervalId=0
    num(): number {
        return 0;
    }
    model(): IModel {
        return model;
    }

    render(): void {

        this.intervalId=setInterval(() => {
            this.createBullet()
            super.renderModels()
        }, config.canvas.fresh)


    }
    protected createBullet() {
        //每个坦克一发炮弹 或多发炮弹

        tank.models.forEach(tank => {
            //some 一个符合条件即true,都不符合条件false
            //every 都符合条件true,
            const bulletNum = this.models.filter(m => m.tank == tank).length;

            if (bulletNum<tank.bulletNum && !tank.stopMove) {

                const x = tank.x + tank.width / 2
                const y = tank.y + tank.height / 2
                const instance = new model(tank);
                this.models.push(instance);

            }
        })
    }
    //增加进入bullet队列
    addBullet(player:IModel){
        const className=this.model();
        const instance =new className(player)
        this.models.push(instance)
    }

}
export default new bullet('bullet')
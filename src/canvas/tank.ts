import config from "../config";
import canvasAbstract from "./canvasAbstract";
import model from '../model/tank';
import position from "../service/position";
class tank extends canvasAbstract implements ICanvas {
    intervalId = 0
    killCount=0
    num(): number {
        return config.tank.num;
    }
    count: number = 0;
  
    model(): IModelController  {
        return model;
    }
   

    render(): void {

        this.intervalId = setInterval(() => {
            this.createModels()
            super.renderModels()
        }, config.canvas.fresh)

    }

    //坦克
    createModels(): void {
        //顶部随机创建

        for (let index = this.models.length; index < config.tank.maxNum; index++) {
            if (this.count == this.num()) return
            const pos = position.randPosition();
            const model = this.model();
        
            const instance = new model(pos.x, 0)
            this.models.push(instance);       
        }

    }
    createSingleModel(): void {
        const pos = position.randPosition();
        const model = this.model();
        const instance = new model(pos.x, 0)
        this.models.push(instance);
    }
}
export default new tank('tank')
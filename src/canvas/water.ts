import config from "../config";
import canvasAbstract from "./canvasAbstract";
import model from '../model/water';
import position from "../service/position";
class water extends canvasAbstract implements ICanvas {
    intervalId: any;
    num(): number {
        return config.water.num;
    }
    model(): IModel {
        return model;
    }
    
    render(): void {
        super.createModels()
        super.renderModels()
    }
       
}
export default new water('water')
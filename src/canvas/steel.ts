import config from "../config";
import canvasAbstract from "./canvasAbstract";
import model from '../model/steel';
import position from "../service/position";
class steel extends canvasAbstract implements ICanvas {
    num(): number {
        return config.steel.num;
    }
    model(): IModel {
        return model;
    }
    init() {
        super.createModels()
        return this;
    }
    render(): void {
        super.renderModels()
    }

    createBossWall() {
        const className = this.model();
        let walls = []

        walls = super.getBossWallPos()
       
        this.models.push(...walls.map(wall => new className(wall.x, wall.y)))
        super.renderModels()
    }

}
export default new steel('steel')
import config from "../config";
import canvasAbstract from "./canvasAbstract";
import model from '../model/wall';
import position from "../service/position";
class wall extends canvasAbstract implements ICanvas {
    intervalId: any;
    num(): number {
        return config.wall.num;
    }
    model(): IModel {
        return model;
    }
    life:number=2
    init(){
        this.createBossWall()
        super.createModels()
        return this;
    }
    render(): void {
       
        super.renderModels()
    }
    createBossWall() {
        const className = this.model();
        let walls = []
        walls=super.getBossWallPos()
        
        
        this.models.push(...walls.map(wall => new className(wall.x, wall.y)))
        super.renderModels()
    }

}
export default new wall('wall')